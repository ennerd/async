<?php
namespace F2;
require("vendor/autoload.php");

setTimeout(function() {
    echo "Timeout\n";
    die();
}, 10000);
function keep_it_going() {
    echo "\r".memory_get_peak_usage();
    defer("F2\keep_it_going");
}

keep_it_going();
