<?php
require('vendor/autoload.php');

use function F2\{setTimeout, clearTimeout, setInterval, clearInterval, queueMicrotask, defer};

// Two tasks running with cooperative multitasking
defer(function() {
    while(true) {
        echo "you > I love you!\n";
        yield;
    }
});
defer(function() {
    while(true) {
        echo "me > I love you too!\n";
        yield;
    }
});
