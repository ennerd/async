<?php
declare(strict_types=1);

namespace F2;
/**
 * # f2/async
 *
 * Makes async code portable between plain PHP and event looped frameworks such
 * as ReactPHP, Amp or Swoole.
 *
 * Provides identical functionality to `setTimeout()`, `clearTimeout()`, `setInterval()`
 * `clearInterval()` and `queueMicrotask()` known from browsers, web workers and node.
 *
 * Serves as a base for building proper implementations of promises and other
 * asynchronous code.
 *
 *
 * ## ReactPHP
 *
 * The defer() method can be configured to use any event loop. For example, to
 * configure it to use the ReactPHP event loop:
 *
 *
 * ```php
 * \F2\globals('f2/config')['defer'] = [ $loop, 'futureTick' ];
 * ```
 *
 *
 * ## Amp
 *
 * ```php
 * \F2\globals('f2/config')['defer'] = [ \Amp\Loop::class, 'defer' ];
 * ```
 *
 *
 * ## Swoole
 *
 * ```php
 * \F2\globals('f2/config')['defer'] = 'swoole_event_defer';
 * ```
 *
 *
 * > *Note!* The reason `'f2/config'` is used above, is that f2/async queries
 *   f2/config for the `'defer'` configuration, and this is simply a way of
 *   overriding the configuration emitted by f2/config for the 'defer' keyword.
 *
 */

/**
 * Add a job to the event queue and launch the queue processor
 *
 * @internal
 */
function _async_defer(callable $deferred) {
    static $launched = false;
    if(!$launched) {
        $launched = true;
        register_shutdown_function('F2\_async_process');
    }

    globals('f2/async', 'deferred')[] = $deferred;
}

function _async_process(int $limit = null):void {
    $count = 0;
    while (($limit === null || $count++ < $limit) && _async_process_next());

die("QUEUE EMPTY");
}

function _async_process_next():bool {
    $queue = &globals('f2/async', 'deferred');
    $job = array_shift($queue);

    if($job !== null) {
        if(!is_callable($job)) {
	        $job();
	}
        return true;
    }
    return false;
}

/**
 * Defers execution of the callable function. The callback will be invoked after
 * all other blocking and previously deferred functions have completed.
 *
 * @param $deferred Callback method.
 */
function defer( callable $deferred ):void {
    static $defer, $queue;
    //$deferred = \Closure::fromCallable($deferred)->bindTo(null);

    if (empty($defer)) {
        $defer = config(
            "defer",
            function() {
                return 'F2\_async_defer';
            }
        );
    }

    /**
     * Generator handler that handles yielded stuff
     */

    $generatorHandler = function(\Generator $generator) { //use (&$generatorHandler) {
        if (!$generator->valid()) {
            return;
        }

        $output = $generator->current();

        // TODO: $output might be a promise

        $generator->send($output);

        // TODO: go again@
/*
        defer(function() use($generatorHandler, $generator) {
            $generatorHandler($generator);
        });
*/
    };

    /**
     * Wrapping in a function that will execute any remaining microtasks
     */
    $deferred = function() use($deferred, $generatorHandler) {
        $globals = &globals('f2/async');
        while(!empty($globals['microtasks'])) {
            $microtask = array_shift($globals['microtasks']);
            // Microtasks can't be coroutines.
            $microtask();
        }
        $generator = $deferred();
        if (is_object($generator) && is_a($generator, \Generator::class)) {
            defer(function() use($generator, $generatorHandler) {
                $generatorHandler($generator);
            });
        }
    };

    call_user_func($defer, $deferred);
}

/**
 * Schedules the callable to be invoked immediately before any other deferred
 * functions.
 *
 * @see https://github.com/fergald/docs/blob/master/explainers/queueMicrotask.md
 * 
 * @param $deferred The callback to execute immediately before any other pending callbacks
 */
function queueMicrotask( callable $deferred ):void {
    $globals = &globals('f2/async');
    $globals['microtasks'][] = $deferred;
    // Deferring a no-op ensures microtasks are enqueued, and we don't need to introduce a way of checking that the loop has stuff to do.
    defer(function(){});
}

/**
 *
 */
function setTimeout( callable $deferred, float $milliseconds = 0 ):int {
    $timers = &globals("f2/async");
    if(!isset($timers["timers"])) {
        $timers += [
            "index" => 0,
            "queue" => $queue = new class extends \SplMinHeap {
                protected function compare($a, $b) : float {
                    if ($b->runAt === $a->runAt) return 0;
                    return $a->runAt < $b->runAt ? 1 : -1;
                }
            },
            "timers" => [],
            "process" => $process = function() use($queue, &$process):void {
                if ($queue->isEmpty()) {
                    return;
                }

                // Avoid busy run by sleeping so that 1 ms has elapsed, ignoring any other deferred jobs
                static $lastRun = null;
                if ($lastRun === null) {
                    $lastRun = microtime(true);
                } else {
                    $thisRun = microtime(true);
                    $elapsed = $thisRun - $lastRun;
                    $lastRun = $thisRun;

                    if($elapsed < 0.0005) {
                        // Very short time has elapsed since last run
                        usleep(500);
                    }
                }

                $t = microtime(true);
                $didSome = false;

                // $queue is a min heap, so we get elements in the proper order and can
                // stop the loop when the topmost element is not ready to run.
                while( !$queue->isEmpty() && $queue->top()->runAt <= $t) {
                    $queue->extract()->run();
                }

                if( !$queue->isEmpty() ) {
                    // More items in queue. Ensure we'll get them done.
                    defer($process);
                }

            },
        ];
    }

    $timer = new class($deferred, microtime(true) + ($milliseconds / 1000)) {
        public $runAt;
        protected $deferred;
        protected $finished = false;
        protected $cancelled = false;

        public function __construct(callable $deferred, float $runAt) {
            $this->deferred = $deferred;
            $this->runAt = $runAt;
        }

        public function run():void {
            if ($this->cancelled) {
                return;
            }
            $this->finished = true;
            defer($this->deferred);
        }

        public function cancel():void {
            $this->cancelled = true;
        }

        public function isCancelled():bool {
            return $this->cancelled;
        }

        public function isFinished():bool {
            return $this->finished;
        }
    };

    $timerId = $timers["index"]++;

    $timers["queue"]->insert($timer);
    $timers["timers"][$timerId] = $timer;

    // Ensure that the queue will be processed. This might cause multipe deferred queue processing tasks, but the cost is minimal.
    defer($timers['process']);

    return $timerId;
}

function clearTimeout( int $id ): void {
    $timers = &globals("f2/async");
    if (!isset($timers["timers"][$id])) {
        return;
    }
    $timers["timers"][$id]->cancel();
    unset($timers["timers"][$id]);
}

function setInterval( callable $callback, float $milliseconds = 0, ...$args ):int {
    $globals = &globals('f2/async');

    $timerId = null;

    /**
     * Might be overly complex. Liked the fun of it. $timerId is a reference, so it can
     * be updated from within the $wrap method. The $timerId is also stored in globals
     * and gets updated there as well, which allows clearInterval to work.
     */
    $wrap = function() use($callback, $milliseconds, $args, &$wrap, &$timerId, &$globals) {
        $timerId = setTimeout( $wrap, $milliseconds, $args );
        defer( $callback );
    };
    $timerId = setTimeout( $wrap, $milliseconds );
    $globals['intervalMap'][$timerId] = &$timerId;
    return $timerId + 0;
}

function clearInterval( int $id ):void {
    $globals = &globals('f2/async');
    if (!array_key_exists($id, $globals['intervalMap'])) {
        return;
    }
    $timeoutId = $globals['intervalMap'][$id];
    clearTimeout($timeoutId);
    unset($globals['intervalMap'][$id]);
}
