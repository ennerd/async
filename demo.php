<?php
require('vendor/autoload.php');

use function F2\{setTimeout, clearTimeout, setInterval, clearInterval, queueMicrotask, defer};

$intervalId = setInterval(function() {
    echo ".";
}, 50);

setTimeout(function() {
    echo "1000 ms has passed\n";
}, 1000);

setTimeout(function() {
    echo "500 ms has passed\n";
}, 500);

setTimeout(function() use($intervalId) {
    clearInterval($intervalId);
    echo "done after 2000 seconds!\n";
}, 2000);


echo "Are you ready to see what comes after me?\n";
