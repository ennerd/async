<?php
require('vendor/autoload.php');

use function F2\{setTimeout, clearTimeout, setInterval, clearInterval, queueMicrotask, defer};

defer(function() {

    echo yield nestedGenerator();;

});


function nestedGenerator() {

    yield;

    return "Frode";

}
