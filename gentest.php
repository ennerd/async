<?php


function frode() {
    echo "before yield\n";
    yield "a";
    echo "after first\n";
    yield "b";
    echo "after second\n";
    yield "c";
    echo "after last\n";
}

$g = frode();

echo "\n";
echo "current=".$g->current()."\n";
echo "valid: ".t($g->valid())."\n";

echo "\n";
$g->next();
echo "current=".$g->current()."\n";
echo "valid: ".t($g->valid())."\n";

echo "\n";
$g->next();
echo "current=".$g->current()."\n";
echo "valid: ".t($g->valid())."\n";

echo "\n";
$g->next();
echo "current=".$g->current()."\n";
echo "valid: ".t($g->valid())."\n";


function t($v) {
    return $v ? 'y' : 'n';
}
