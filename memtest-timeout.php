<?php
namespace F2;
require("vendor/autoload.php");

ini_set('memory_limit', 7000000);

setTimeout(function() {
    echo "Timeout\n";
    die();
}, 100000);


function keep_it_going() {
    static $ps, $c = 0;

    if($ps !== time()){
        echo " $c \n";
        $ps = time();
        $c = 0;
    }
    $c++;

    echo "\r".memory_get_peak_usage();
    setTimeout("F2\keep_it_going", 0);
}

keep_it_going();
