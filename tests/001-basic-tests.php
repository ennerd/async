<?php
namespace F2;

include(__DIR__."/../vendor/autoload.php");

use function F2\asserty;

$assertions = [];
$order = [];
$assertions['defer'] = false;
defer(function() {
    global $assertions, $order;
    $order[] = 'defer';
    $assertions['defer'] = true;
});

$assertions['queue-microtask'] = false;
queueMicrotask(function() {
    global $assertions, $order;
    $order[] = 'queue-microtask';
    asserty($assertions['defer'] === false, "queueMicrotask executed after deferred task");
    $assertions['queue-microtask'] = true;
});

$assertions['interval-5-times'] = false;
$intervalId = setInterval(function() use(&$intervalId) {
    global $assertions, $order;
    static $count = 0;
    $order[] = "interval-$count";
    if ($assertions['interval-5-times'] = ($count == 4)) {
        clearInterval($intervalId);
    }
    $count++;
}, 78);

/**
 * Check that this timeout is invoked
 */
$assertions['timeout-remove-immediately-300'] = false;
setTimeout(function() {
    global $assertions, $order;
    $order[] = 'timeout-remove-immediately-300';
    $assertions['timeout-remove-immediately-300'] = true;
}, 300);

/**
 * Check that timeouts can be removed immediately
 */
$assertions['timeout-removal'] = true;
$id = setTimeout(function() {
    global $assertions, $order;
    $order[] = 'timeout-removal';
    $assertions['timeout-removal'] = false;
});
clearTimeout($id);

/**
 * Check that it is possible to remove a timeout from
 * the event loop after a time.
 */
$assertings['deferred-timeout-removal'] = true;
$id = setTimeout(function() {
    global $assertions, $order;
    $order[] = 'deferred-timeout-removal';
    $assertions['deferred-timeout-removal'] = false;
}, 200);
setTimeout(function() use($id) {
    global $assertions, $order;
    $order[] = 'deferred-timeout-removal-done';
    clearTimeout($id);
}, 100);

/**
 * Check that all assertions were successful
 */
setTimeout(function() {
    global $assertions, $order;
    $order[] = 'assertion-check';
    foreach($assertions as $k => $assertion) {
        asserty($assertion, $k." failed");
    }

    asserty(
        serialize($order) == serialize(["queue-microtask","defer","interval-0","deferred-timeout-removal-done","interval-1","interval-2","timeout-remove-immediately-300","interval-3","interval-4","assertion-check"]),
        'Incorrect order'
    );

    $assertions['all-ok'] = true;
}, 1000);

/*
ob_start(function($c) {
    global $assertions, $order;

    return $c.(!empty($assertions['all-ok']) ? "ALL OK\n" : "");
});
*/
