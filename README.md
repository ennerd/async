# f2/async

Makes async code portable between plain PHP and event looped frameworks such
as ReactPHP, Amp or Swoole.

Provides identical functionality to `setTimeout()`, `clearTimeout()`, `setInterval()`
`clearInterval()` and `queueMicrotask()` known from browsers, web workers and node.

Serves as a base for building proper implementations of promises and other
asynchronous code.

## Simple example

First install f2/async:

```shell
$ composer require f2/async
```

Then create a file `test.php`:

```php
<?php
require('vendor/autoload.php');

use function F2\{setTimeout, clearTimeout, setInterval, clearInterval, queueMicrotask, defer};

$intervalId = setInterval(function() {
    echo ".";
}, 50);

setTimeout(function() {
    echo "1000 ms has passed\n";
}, 1000);

setTimeout(function() {
    echo "500 ms has passed\n";
}, 500);

setTimeout(function() use($intervalId) {
    clearInterval($intervalId);
    echo "done after 2000 seconds!\n";
}, 2000);

echo "Are you ready to see what comes after me?\n";
```

Then run it:

```shell
$ php test.php
Are you ready to see what comes after me?
.........500 ms has passed
..........1000 ms has passed
....................done after 2000 seconds!
```

## Then the cool stuff



## Using some more *enveloping*, *encircling*, *

## ReactPHP

The defer() method can be configured to use any event loop. For example, to
configure it to use the ReactPHP event loop:


```php
\F2\globals('f2/config')['defer'] = [ $loop, 'futureTick' ];
```


## Amp

```php
\F2\globals('f2/config')['defer'] = [ \Amp\Loop::class, 'defer' ];
```


## Swoole

```php
\F2\globals('f2/config')['defer'] = 'swoole_event_defer';
```


> *Note!* The reason `'f2/config'` is used above, is that f2/async queries
  f2/config for the `'defer'` configuration, and this is simply a way of
  overriding the configuration emitted by f2/config for the 'defer' keyword.


