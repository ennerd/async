<?php
require('vendor/autoload.php');

use function F2\{setTimeout, clearTimeout, setInterval, clearInterval, queueMicrotask, defer};

defer(function() {

    $client = new \GuzzleHttp\Client();
    
    $promise = $client->sendAsync(new \GuzzleHttp\Psr7\Request('GET', 'http://httpbin.org'));

    var_dump($promise);


    
});
